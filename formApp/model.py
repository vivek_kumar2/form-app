from formApp import db, login_manager
from flask_login import UserMixin
from datetime import datetime


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    site = db.Column(db.String(30), nullable=False)
    region = db.Column(db.String(20), nullable=False)
    role = db.Column(db.String(20), nullable=False)
    password = db.Column(db.String(60), nullable=False)
    ftr = db.relationship('Ftr', backref='author', lazy=True)


    def __repr__(self):
        return f"User('{self.username}','{self.email}', '{self.site}','{self.role}'')"

class Ftr(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_class = db.Column(db.String(20), nullable=False)
    product_type = db.Column(db.String(20), nullable=False)
    idh = db.Column(db.String(20), nullable=False)
    product_name = db.Column(db.String(50), nullable=False)
    batch = db.Column(db.String(20), nullable=True)
    production_line = db.Column(db.String(20), nullable=True)
    characterstics = db.Column(db.String(20), nullable=False)
    non_ftr_tons = db.Column(db.Float(), nullable=False)
    non_ftr_cost = db.Column(db.Float(), nullable=False)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return f"ftr('{self.product_class}','{self.idh}','{self.date}')"