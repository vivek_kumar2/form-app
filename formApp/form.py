from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, ValidationError, FloatField, SelectField
from wtforms.validators import DataRequired, Length, EqualTo, Email, NumberRange
from formApp.model import User
class RegistrationForm(FlaskForm):
    username = StringField('Username', 
                            validators=[DataRequired(), Length(min=3, max=20)])
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    
    site = SelectField('Site', choices=[], validators=[DataRequired()])

    role = SelectField('Role', choices = [('user','User'),('admin','Admin')], validators=[DataRequired()])
    
    password = PasswordField('Password',
                            validators=[DataRequired(), Length(min=4, max=10)])
    confirm_password = PasswordField('Confirm Password',
                                validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign Up')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('Username exist. Please choose different username!')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('Email exist. Please choose different email!')

class LoginForm(FlaskForm):
    email = StringField('Email',
                        validators=[DataRequired(), Email()])
    password = PasswordField('Password',
                            validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    submit = SubmitField('Login')

class FtrForm(FlaskForm):
    product_class = SelectField('Product Class', choices=[], validators=[DataRequired()])
    product_type = SelectField('Product Type', choices=[], validators=[DataRequired()])
    idh = StringField('IDH', validators=[DataRequired()])
    product_name = StringField('Product Name', validators=[DataRequired()])
    production_line = StringField('Production Line')
    batch = StringField('Batch Number')
    characterstics = SelectField('Characterstics', validate_choice=False, validators=[DataRequired()])
    non_ftr_tons = FloatField('Non FTR (tons)', validators=[DataRequired(), NumberRange(min=0.0, message='Enter valid data')])
    non_ftr_cost = FloatField('Non FTR Cost (EUR)', validators=[DataRequired(), NumberRange(min=0.0)])
    submit = SubmitField('Submit')