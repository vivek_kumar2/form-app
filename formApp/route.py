from flask import render_template, url_for, flash, redirect, jsonify
from flask_login import login_user, current_user, logout_user, login_required
from formApp.form import RegistrationForm, LoginForm, FtrForm
from formApp.model import User, Ftr
from formApp import app, db, bcrypt
from formApp.config import field, arbitary_data

@app.route("/")
@app.route("/home")
@login_required
def home():
    return render_template("home.html", title = 'Home', apps=arbitary_data.apps())

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    site = arbitary_data.site()
    form.site.choices = [(site,site) for site in site['Sites']]
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username = form.username.data, 
                    email = form.email.data, 
                    site = form.site.data,
                    region = site[site['Sites'] == form.site.data]['Region'].values[0],
                    role = form.role.data,
                    password = hashed_password)
        db.session.add(user)
        db.session.commit()
        flash(f'Account created for {form.username.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('register.html', title='Register', form=form, fields=field.register_fields)
    
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            return redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route("/ftr", methods=['GET', 'POST'])
@login_required
def ftr():
    form = FtrForm()
    form.product_class.choices = [(prod_class, prod_class) for prod_class in arbitary_data.product_class_options]
    form.product_type.choices = [(prod_type, prod_type) for prod_type in arbitary_data.product_type_options]
    form.characterstics.choices = [(character, character) for character in arbitary_data.characterstics('Bulk')]
    if form.validate_on_submit():
        print ("I am here!!!")
        ftr_data = Ftr(product_class = form.product_class.data, 
                        product_type = form.product_type.data, 
                        idh = form.idh.data,
                        product_name = form.product_name.data,
                        batch = form.batch.data,
                        characterstics = form.characterstics.data,
                        non_ftr_tons = form.non_ftr_tons.data,
                        non_ftr_cost = form.non_ftr_cost.data,
                        author = current_user)
        db.session.add(ftr_data)
        db.session.commit()
        flash(f'Data added!!', 'success')
        return redirect(url_for('success'))
    else:
        print(form.errors)
        print(form.characterstics.data)
        print(type(form.characterstics.data))
    return render_template("ftr.html", title='First Time Right', form=form, fields=field.ftr_fields)

@app.route('/charac/<product_class>')
def charac(product_class):

    charactersticsArray = []
    for character in arbitary_data.characterstics(product_class):
        charac = {}
        charac['Characterstics'] = character
        charactersticsArray.append(charac)

    
    return jsonify({product_class : charactersticsArray})

@app.route('/success')
def success():
    return render_template("success.html", title='Success!')

@app.route('/ftr_dashboard')
def ftr_dashboard():
    return render_template("ftr_dashboard.html", title='FTR Dashboard')

